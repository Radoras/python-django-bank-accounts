from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from .models import User, Account


class AccountAdmin(admin.ModelAdmin):
    list_display = ('id', 'user', 'funds')
    fieldsets = (
        (None, {
            'fields': ('id', 'user', 'funds')
        }),
    )
    raw_id_fields = ('user', )


admin.site.register(User, UserAdmin)
admin.site.register(Account, AccountAdmin)
