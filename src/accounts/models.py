from django.contrib.auth.models import UserManager, PermissionsMixin, AbstractUser
from django.db import models

from common import ActualCharField


class User(AbstractUser, PermissionsMixin):
    username = ActualCharField(primary_key=True, max_length=8)

    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    email = models.EmailField()

    is_admin = models.BooleanField(default=False)
    is_staff = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)

    date_joined = AbstractUser.date_joined

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = []

    objects = UserManager()


class Account(models.Model):
    id = ActualCharField(primary_key=True, max_length=26)
    user = models.ForeignKey(User, related_name='account_user', on_delete=models.CASCADE)
    funds = models.FloatField(default=0)
