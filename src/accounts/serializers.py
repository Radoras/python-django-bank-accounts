from django.contrib.auth.hashers import make_password
from rest_framework import serializers

from .models import User, Account


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'email', 'is_staff')


class CreateOrEditUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('username', 'password', 'first_name', 'last_name', 'email', 'is_staff')

    def create(self, validated_data):
        validated_data['password'] = make_password(validated_data.get('password'))
        return super(CreateOrEditUserSerializer, self).create(validated_data)


class AccountSerializer(serializers.ModelSerializer):
    user = UserSerializer('user')

    class Meta:
        model = Account
        fields = ('id', 'user', 'funds')


class CreateAccountSerializer(serializers.ModelSerializer):
    class Meta:
        model = Account
        fields = ('id',)


class EditAccountSerializer(serializers.ModelSerializer):
    class Meta:
        model = Account
        fields = ('funds',)
