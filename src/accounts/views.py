from rest_framework import viewsets
from rest_framework.response import Response

from accounts.models import User, Account
from accounts.serializers import UserSerializer, AccountSerializer, EditAccountSerializer, \
    CreateAccountSerializer, CreateOrEditUserSerializer


class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()

    def get_serializer_class(self):
        match self.action:
            case 'create' | 'update': return CreateOrEditUserSerializer
            case _: return UserSerializer

    def get_queryset(self):
        return self.queryset


class AccountViewSet(viewsets.ModelViewSet):
    queryset = Account.objects.all()

    def get_serializer_class(self):
        match self.action:
            case 'create': return CreateAccountSerializer
            case 'update': return EditAccountSerializer
            case _: return AccountSerializer

    def get_queryset(self):
        return self.queryset

    def create(self, request, *args, **kwargs):
        logged_user = request.user

        if len(account_id := request.data['id']) != 26:
            raise ValueError('Account ID length must be equal to 26!')

        new_account = Account.objects.create(id=account_id,
                                             user=logged_user,
                                             funds=0)
        new_account.save()

        serializer = CreateAccountSerializer(new_account)
        return Response(serializer.data)
