from rest_framework import serializers

from accounts.serializers import AccountSerializer
from .models import Transaction


class TransactionSerializer(serializers.ModelSerializer):
    sender = AccountSerializer('sender')
    receiver = AccountSerializer('receiver')

    class Meta:
        model = Transaction
        fields = ('pk', 'sender', 'receiver', 'amount', 'title', 'execution')


class SendTransactionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Transaction
        fields = ('sender', 'receiver', 'amount', 'title', 'execution')
