from datetime import date, datetime

from django.db import transaction
from rest_framework import mixins
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet

from accounts.models import Account
from transactions.models import Transaction
from transactions.permissions import IsSenderOrReadOnly
from transactions.serializers import TransactionSerializer, SendTransactionSerializer


class TransactionViewSet(mixins.CreateModelMixin,
                         mixins.RetrieveModelMixin,
                         mixins.ListModelMixin,
                         GenericViewSet):
    queryset = Transaction.objects.all()
    permission_classes = (IsSenderOrReadOnly,)

    def get_serializer_class(self):
        match self.action:
            case 'create': return SendTransactionSerializer
            case _: return TransactionSerializer

    def get_queryset(self):
        return self.queryset

    def create(self, request, *args, **kwargs):
        sender = Account.objects.get(id=request.data['sender'])
        receiver = Account.objects.get(id=request.data['receiver'])
        execution = (datetime.strptime(exec, "%Y-%m-%d").date()
                     if (exec := request.data['execution'])
                     else date.today())

        if sender.funds < (amount := float(request.data['amount'])):
            raise ValueError('Sender does not have enough funds!')

        sender.funds -= amount
        receiver.funds += amount
        new_transaction = Transaction.objects.create(sender=sender,
                                                     receiver=receiver,
                                                     amount=amount,
                                                     title=request.data['title'],
                                                     execution=execution)

        with transaction.atomic():
            sender.save()
            receiver.save()
            new_transaction.save()

        serializer = SendTransactionSerializer(new_transaction)
        return Response(serializer.data)
