from django.db import models

from accounts.models import Account


class Transaction(models.Model):
    sender = models.ForeignKey(Account, related_name='transaction_sender', on_delete=models.RESTRICT)
    receiver = models.ForeignKey(Account, related_name='transaction_receiver', on_delete=models.RESTRICT)
    amount = models.FloatField()
    title = models.CharField(max_length=255)
    execution = models.DateField(blank=True)

    class Meta:
        constraints = [
            models.CheckConstraint(
                name="%(app_label)s_%(class)s_different_sender_receiver",
                check=(~models.Q(sender=models.F("receiver")))
            )
        ]
