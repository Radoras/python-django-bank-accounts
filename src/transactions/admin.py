from django.contrib import admin

from .models import Transaction


class TransactionAdmin(admin.ModelAdmin):
    list_display = ('sender', 'receiver', 'amount', 'title', 'execution')
    readonly_fields = ('id', )
    fieldsets = (
        (None, {
            'fields': ('sender', 'receiver', 'amount', 'title', 'execution')
        }),
    )
    raw_id_fields = ('sender', 'receiver')


admin.site.register(Transaction, TransactionAdmin)
