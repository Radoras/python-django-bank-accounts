from django.contrib import admin
from django.urls import path
from rest_framework.routers import SimpleRouter

from accounts.views import UserViewSet, AccountViewSet
from transactions.views import TransactionViewSet

router = SimpleRouter(trailing_slash=False)
router.register(r'users', UserViewSet, basename='users')
router.register(r'accounts', AccountViewSet, basename='accounts')
router.register(r'transactions', TransactionViewSet, basename='transactions')

urlpatterns = [
    path('admin/', admin.site.urls),
]
urlpatterns += router.urls
